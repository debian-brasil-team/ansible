# jibri

This role sets up a Jibri streaming server for streaming Jitsi Meet meetings.

## Tasks

The tasks are divided into:

* `chrome.yml`: Installs Chrome and chrome-driver
* `kernel.yml`: Ensures the correct kernel is installed and enables required modules
* `java.yml`: Installs Java 8, required by Jibri
* `jibri.yml`: Manages Jibri configuration

## Available variables

The main variables for this role are:

* `debian_version`: Version of Debian, when using Debian.
* `jitsi_meet_domain`: Domain that users use to connect to the Jitsi Meet server
* `jibri.voctomix_endpoints`: Dictionary of room names to Voctomix TCP endpoints, that will be streamed to.
                              Syntax: ffmpeg URL.
* `jibri.streaming_keys`: Dictionary of room names to streaming keys.
                          The key entered is entered by the Jitsi Meet user to secure streaming.
* `jibri.user`: The username Jibri will authenticate itself to prosody with. Defaults to jibri
* `jibri.password`: The password Jibri will authenticate itself to prosody with
* `jibri.recorder_user`: The username Jibri will authenticate itself to prosody with during a call it is streaming. Defaults to recorder
* `jibri.recorder_password`: The password Jibri will authenticate itself to
  prosody with during a call it is streaming
