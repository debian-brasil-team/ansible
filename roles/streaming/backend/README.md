# streaming/backend

Install and configure nginx to act as a streaming backend server.

## Tasks

Everything is in the `tasks/main.yml` file.

## Available variables

Main variables are:

* `debian_version`:                Codename of the version of Debian used.

* `streaming.backend.data_root`:   nginx data_root.

* `streaming.adaptive`:            configuration of HLS encoding variants

    * `video_codec`: HLS video codec. Should probably be `libx264`

    * `audio_codec`: HLS audio codec. Should probably be `aac`

    * `variants`: a description of all the variants. Should be an object; each attribute is another object with the below items. The name of the attribute will be used as the name of the stream.

        * `hls_bandwidth`: the bandwidth as configured in the HLS manifest

        * `video_bitrate`: the video bitrate as passed to ffmpeg

        * `audio_bitrate`: the audio bitrate as passed to ffmpeg

        * `extra_settings`: any extra settings to pass to ffmpeg for this video stream

        * `width`: the width of this stream, used for scaling (height is computed)

* `streaming.hq_config`:           configuration of main HLS variant

    * `keyframe_period`: how much time (in seconds) between key frames for the HLS stream

    * `width`: width (in pixels) of the source video stream

    * `hls_bandwidth`: the bandwidth of the "source" video stream as it will be stored in the HLS manifest

    * `video_bitrate`: the bitrate for the "source" video stream as passed to ffmpeg for transcoding

    * `audio_bitrate`: the bitrate for the "source" audio stream as passed to ffmpeg for transcoding

* `streaming.dash`:                configuration of DASH encoding variants

    * `video_codec`: the video codec for streams in the DASH manifest (e.g., `libsvtav1` for AV1)

    * `audio_codec`: the audio codec for streams in the DASH manifest (e.g., 'libopus' for Opus)

    * `preset`: the parameter used for the `-preset` parameter to ffmpeg (e.g., `8` for AV1 without dropping frames on modern hardware)

    * `video_variants`: an array of objects. Each object can have the following attributes, all of which are optional (in which case the relevant parameter is not passed to ffmpeg):

        * `crf`: the value of the `-crf` parameter passed to ffmpeg for that stream (e.g., `23`)

        * `scale`: the value of the `-s` parameter passed to ffmpeg for that stream (e.g., `640x360` to scale a 720p video to half resolution)

        * `bitrate`: the value of the `-b` parameter passed to ffmpeg for that stream

        * `maxrate`: the value of the `-maxrate` parameter passed to ffmpeg for that stream

    * `audio_variants`: and array of objects. Same system as for `video_variants`, except that only one object parameter is supported: `bitrate`, for the audio bit rate for that alternative.

* `streaming.backend.server_name`: The FQDN of your backend streaming server.

* `streaming.rooms`:               List. The name of the different rooms you are
                                   recording in. This will end up in the URL of
                                   the stream available.

* `streaming.youtube_stream_keys`: Optional, a dictionary of room names to youtube streaming keys.
                                   The stream will be mirrored to YouTube.

Other variables used are:

* `letsencrypt_well_known_dir`: Directory where to store the `/.well-known/` data
                                for the Let's Encrypt challenge.

* `skip_unit_test`:  Used internally by the test suite to disable actions that
                     can't be performed in the gitlab-ci test runner.
