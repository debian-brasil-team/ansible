# jitsi

This role sets up a Jitsi server ready for clients to create rooms and have
meetings.

## Tasks

Everything is in the `tasks/main.yml` file.

## Available variables

The main variables for this role are:

* `jitsi_meet_domain`: Domain that users use to connect to the Jitsi Meet server

* `jigasi.caller_id`: The Caller ID to provide on outbound calls
* `jigasi.conference_mapper`: URL for the Jitsi Conference Mapper API.
* `jigasi.dial_out_trunk`: Name of the trunk to use for all outbound calls. Disabled if not specified.
* `jigasi.numbers`: Dictionary of phone numbers to advertise to users.
                    Key: Country Code, Value: List of phone numbers.
* `jigasi.password`: The password Jigasi will authenticate itself to Asterisk with.
* `jigasi.trunks`: A list of SIP trunks for inbound/outbound calls.
* `jigasi.trunks.[].transport`: SIP transport (`tls` or `udp`).
* `jigasi.trunks.[].server`: SIP server name.
* `jigasi.trunks.[].realm`: SIP authentication realm name.
* `jigasi.trunks.[].username`: SIP username.
* `jigasi.trunks.[].password`: SIP password.
* `jigasi.trunks.[].extension`: SIP incoming extension (probably the DID).
* `jigasi.trunks.[].sdes`: Boolean. Encrypt RTP voice streams.
* `jigasi.trunks.[].codecs`: List of strings. Codec names permitted on this trunk.
* `jigasi.xmpp_user`: The username Jigasi will authenticate itself to prosodoy with. Defaults to jigasi.
* `jigasi.xmpp_password`: The password Jigasi will authenticate itself to prosodoy with.

* `jibri.password`: The password Jibri will authenticate itself to prosody with
* `jibri.user`: The username Jibri will authenticate itself to prosody with. Defaults to jibri
* `jibri.recorder_password`: The password Jibri will authenticate itself to prosody with during a call it is streamin
* `jibri.recorder_user`: The username Jibri will authenticate itself to prosody with during a call it is streaming. Defaults to recorder
