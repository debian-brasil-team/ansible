# opsis

Configures a machine as an Opsis capture PC.

Also supports Black Magic Web Presenter devices, that present a similar
v4l USB device.

## Tasks

Tasks are separated in two different parts:

* `tasks/opsis.yml` manages the tools to setup up and connect to the opsis.

* `tasks/uvc.yml` installs a udev rule to grant permissions to known
  HDMI UVC USB devices. e.g. Blackmagic Web Presenter.

* `tasks/hdmi2usbmond.yml` manages the HDMI2USB monitoring tools.

## Available variables

Main variables are:

* `serial_terminal`: List. Installs serial terminal packages that are in Debian.
                     The first one listed will be used for the `opsis` command
                     used to connect to the opsis board.

* `debian_version`:  Version of Debian, when using Debian.

Other variables used are:

* `skip_unit_test`:  Used internally by the test suite to disable actions that
                     can't be performed in the gitlab-ci test runner.
